#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_ADVICE ::..")

PROJECT(plugin_advice)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skg_advice_SRCS 
	skgadviceplugin.cpp
	skgadviceboardwidget.cpp
	skgtipofdayboardwidget.cpp
)

ki18n_wrap_ui(skg_advice_SRCS skgtipofdayboardwidget.ui)

ADD_LIBRARY(skg_advice MODULE ${skg_advice_SRCS})
TARGET_LINK_LIBRARIES(skg_advice KF5::Parts KF5::ConfigWidgets skgbasemodeler skgbasegui)

########### install files ###############
INSTALL(TARGETS skg_advice DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skg-plugin-advice.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skg_advice.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skg_advice )
