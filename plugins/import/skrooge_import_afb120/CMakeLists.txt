#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_AFB120 ::..")

PROJECT(plugin_import_AFB120)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_afb120_SRCS
	skgimportpluginafb120.cpp
)

ADD_LIBRARY(skrooge_import_afb120 MODULE ${skrooge_import_afb120_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_afb120 KF5::Parts skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_afb120 DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-afb120.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

