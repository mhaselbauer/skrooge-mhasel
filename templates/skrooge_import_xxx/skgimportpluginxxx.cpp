/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
/** @file
 * This file is Skrooge plugin for XXX import / export.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgimportpluginxxx.h"

#include <klocalizedstring.h>
#include <kpluginfactory.h>


#include "skgtraces.h"

/**
 * This plugin factory.
 */
K_PLUGIN_FACTORY(SKGImportPluginXXXFactory, registerPlugin<SKGImportPluginXXX>();)

SKGImportPluginXXX::SKGImportPluginXXX(QObject* iImporter, const QVariantList& iArg)
    : SKGImportPlugin(iImporter)
{
    SKGTRACEINFUNC(10)
    Q_UNUSED(iArg)
}

SKGImportPluginXXX::~SKGImportPluginXXX()
{
}

bool SKGImportPluginXXX::isImportPossible()
{
    SKGTRACEINFUNC(10)
    return (!m_importer ? true : m_importer->getFileNameExtension() == "XXX");
}

SKGError SKGImportPluginXXX::importFile()
{
    return SKGImportPlugin::importFile();
}

bool SKGImportPluginXXX::isExportPossible()
{
    SKGTRACEINFUNC(10)
    return isImportPossible();
}

SKGError SKGImportPluginXXX::exportFile()
{
    return SKGImportPlugin::exportFile();
}

QString SKGImportPluginXXX::getMimeTypeFilter() const
{
    return "*.xxx|" % i18nc("A file format", "XXX file");
}


